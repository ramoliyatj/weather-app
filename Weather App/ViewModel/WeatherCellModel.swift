//
//  WeatherViewModel.swift
//  Weather App
//
//  Created by Tushar Ramoliya on 14/05/19.
//  Copyright © 2019 Tushar Ramoliya. All rights reserved.
//

import Foundation
import UIKit

class WeatherCellModel {
    
    let cityName : String
    let temp : Double
    let min_temp : Double
    let max_temp : Double
    let main : String
    let icon : String
    
    
    init(weatherMO : WeatherMO) {
        self.cityName = weatherMO.name ?? ""
        self.temp = weatherMO.temp
        self.min_temp = weatherMO.temp_min
        self.max_temp = weatherMO.temp_max
        self.icon = weatherMO.icon ?? ""
        self.main = weatherMO.main ?? ""
    }
    
    
}
