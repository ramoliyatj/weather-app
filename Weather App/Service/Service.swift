//
//  Service.swift
//  Weather App
//
//  Created by Tushar Ramoliya on 14/05/19.
//  Copyright © 2019 Tushar Ramoliya. All rights reserved.
//

import Foundation
import SVProgressHUD

class Service: NSObject {
    static let shared = Service()
    
    func fetchWeather(_ cityID : String,completion: @escaping (WeatherM?, Error?) -> ()) {
        
        SVProgressHUD.setDefaultMaskType(.black)
        SVProgressHUD.show()
        
        let urlString = kBaseURL + cityID + kAPPENDTRAIL
        guard let url = URL(string: urlString) else { return }
        URLSession.shared.dataTask(with: url) { (data, resp, err) in
            
            SVProgressHUD.dismiss()
            if let err = err {
                completion(nil, err)
                print("Failed to fetch courses:", err)
                return
            }
            
            // check response
            
            guard let data = data else { return }
            do {
                
                let weather = try JSONDecoder().decode(WeatherM.self, from: data)
                DispatchQueue.main.async {
                    completion(weather, nil)
                }
            } catch let jsonErr {
                print("Failed to decode:", jsonErr)
            }
            }.resume()
    }
}
