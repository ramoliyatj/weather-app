//
//  WeatherDetailVC.swift
//  Weather App
//
//  Created by Tushar Ramoliya on 15/05/19.
//  Copyright © 2019 Tushar Ramoliya. All rights reserved.
//

import UIKit

class WeatherDetailVC: UIViewController {

    @IBOutlet weak var lblMinimumTemperature: UILabel!
    
    @IBOutlet weak var lblMaximumTemperature: UILabel!
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var imgWeatherIcon: UIImageView!
    @IBOutlet weak var lblWeatherCondition: UILabel!
    @IBOutlet weak var lblTempreture: UILabel!
    
    var weatherObj : WeatherCellModel!
    
    fileprivate func setData() {
        
        self.view.backgroundColor = UIColor.rgb(r: 50, g: 199, b: 242)
        
        self.lblCityName.text = weatherObj.cityName
        self.lblTempreture.text = "\(weatherObj.temp)"
        self.lblWeatherCondition.text = weatherObj.main
        self.imgWeatherIcon.image = UIImage(named: weatherObj.icon)
        self.lblMinimumTemperature.text = "\(weatherObj.min_temp)"
        self.lblMaximumTemperature.text = "\(weatherObj.max_temp)"
    }
    
    fileprivate func setUpNavigationBar() {
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = false
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpNavigationBar()
        setData()
        
    }
    

   

}
