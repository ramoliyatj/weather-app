//
//  WeatherCell.swift
//  Weather App
//
//  Created by Tushar Ramoliya on 14/05/19.
//  Copyright © 2019 Tushar Ramoliya. All rights reserved.
//

import UIKit

class WeatherCell: UITableViewCell {

    
    @IBOutlet weak var lblCityName: UILabel!
    @IBOutlet weak var lblTemp: UILabel!
    
    var weatherModel : WeatherCellModel!{
        didSet {
            self.lblCityName.text = weatherModel.cityName
            self.lblTemp.text = "\(weatherModel.temp) °C"
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.selectionStyle = .none
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
