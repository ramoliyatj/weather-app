//
//  Weather.swift
//  Weather App
//
//  Created by Tushar Ramoliya on 14/05/19.
//  Copyright © 2019 Tushar Ramoliya. All rights reserved.
//

import Foundation


struct WeatherM : Decodable {
    
    let id : Int?
    let name : String?
    let cod : Int?
    let base : String?
    let visibility : Int?
    let dt : Int?
    
    let weather : [Weather]?
    let main : Main?
//    let wind : Wind?
//    let clouds : Clouds?
//    let sys : Sys?
//    let coord : Coord?
    
    
    
}

struct Coord : Decodable {
    let lon : Double?
    let lat : Double?
}

struct Weather : Decodable {
    let id : Int?
    let main : String?
    let description : String?
    let icon : String?
    
}

struct Sys : Decodable {
    let id : Int?
    let type : Int?
    let message : Double?
    let country : String?
    let sunrise : Int?
    let sunset : Int?
    
}

struct Clouds : Decodable {
    let all : Int?
}

struct Wind : Decodable{
    let speed : Double?
    let deg : Int?
}

struct Main : Decodable {
    let temp : Double?
    let pressure : Double?
    let humidity : Double?
    let temp_min : Double?
    let temp_max : Double?
}
