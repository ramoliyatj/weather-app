//
//  ViewController.swift
//  Weather App
//
//  Created by Tushar Ramoliya on 14/05/19.
//  Copyright © 2019 Tushar Ramoliya. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate {
    
    @IBOutlet weak var weatherTableView: UITableView!
    
    let cityIDList = [kSYDNEYID,kMELBOURNEID,kBRISBANEID]
    var isDataSave = false
    let kWeatherCellID = "WeatherCell"
    
    var timer = Timer()
    let simpleOver = SimpleOver()
    
    lazy var fetchedhResultController: NSFetchedResultsController<NSFetchRequestResult> = {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: WeatherMO.self))
        fetchRequest.sortDescriptors = [NSSortDescriptor(key: "name", ascending: true)]
        let frc = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: CoreDataStack.sharedInstance.persistentContainer.viewContext, sectionNameKeyPath: nil, cacheName: nil)
        frc.delegate = self
        return frc
    }()
    
    fileprivate func setUpTableView() {
        self.weatherTableView.delegate = self
        self.weatherTableView.dataSource = self
        self.weatherTableView.register(UINib(nibName: kWeatherCellID, bundle: nil), forCellReuseIdentifier: kWeatherCellID)
    }
    
    
    fileprivate func setupNavBar() {
        navigationItem.title = "Weather"
        navigationController?.delegate = self
        if #available(iOS 11.0, *) {
            navigationController?.navigationBar.prefersLargeTitles = true
            navigationController?.navigationBar.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
        } else {
            // Fallback on earlier versions
        }
        navigationController?.navigationBar.backgroundColor = .yellow
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.rgb(r: 50, g: 199, b: 242)
        
    }
    
    
    fileprivate func fetchWeather(){
        
        do {
            try self.fetchedhResultController.performFetch()
        } catch let error  {
            print("ERROR: \(error)")
        }
        
        self.clearCoreData()
        for (index,i) in self.cityIDList.enumerated(){
            Service.shared.fetchWeather(i) { (weather, err) in
                self.saveInCoreDataWith(weather!)
                if index == 2{
                    self.isDataSave = true
                }
            }
        }
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpTableView()
        setupNavBar()
        fetchWeather()
        timer = Timer.scheduledTimer(timeInterval: 300, target: self, selector: #selector(timerCall), userInfo: nil, repeats: true)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        if #available(iOS 11.0, *) {
            self.navigationController?.navigationBar.prefersLargeTitles = true
        } else {
            // Fallback on earlier versions
        }
    }
    
    @objc func timerCall(){
        print("<----------Timer Call--------->")
        fetchWeather()
    }
    
    
    //NavigationController Delegate Methods
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        simpleOver.popStyle = (operation == .pop)
        return simpleOver
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let count = fetchedhResultController.sections?.first?.numberOfObjects {
            return count
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.weatherTableView.dequeueReusableCell(withIdentifier: kWeatherCellID) as! WeatherCell
        
        if let weatherObj = fetchedhResultController.object(at: indexPath) as? WeatherMO {
            
            cell.weatherModel = WeatherCellModel(weatherMO: weatherObj)
        }
        
        return cell
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if let weatherObj = fetchedhResultController.object(at: indexPath) as? WeatherMO {
            let weatherDetailVC = self.storyboard?.instantiateViewController(withIdentifier: "WeatherDetailVC") as! WeatherDetailVC
            weatherDetailVC.modalTransitionStyle = .crossDissolve
            weatherDetailVC.weatherObj = WeatherCellModel(weatherMO: weatherObj)
            self.navigationController?.pushViewController(weatherDetailVC, animated: true)
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    //Data Saving
    private func saveInCoreDataWith(_ weather : WeatherM) {
        _ = self.createWeatherMOEntityFrom(weather)
        do {
            try CoreDataStack.sharedInstance.persistentContainer.viewContext.save()
        } catch let error {
            print(error)
        }
    }
    
    private func createWeatherMOEntityFrom(_ weather : WeatherM) -> NSManagedObject? {
        
        let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
        if let weatherMoEntity = NSEntityDescription.insertNewObject(forEntityName: "WeatherMO", into: context) as? WeatherMO {
            guard let weatherObj = weather.weather?[0] else{
                return nil
            }
            
            guard let main = weather.main else{
                return nil
            }
            
            guard let id = weather.id, let name = weather.name, let icon = weatherObj.icon, let condition = weatherObj.main, let temp = main.temp, let pressure = main.pressure, let humidity = main.humidity, let temp_min = main.temp_min, let temp_max = main.temp_max else{
                return nil
            }
            
            weatherMoEntity.id = Int64(id)
            weatherMoEntity.name = name
            weatherMoEntity.icon = icon
            weatherMoEntity.temp = temp
            weatherMoEntity.main = condition
            weatherMoEntity.pressure = Int64(pressure)
            weatherMoEntity.humidity = Int64(humidity)
            weatherMoEntity.temp_min = temp_min
            weatherMoEntity.temp_max = temp_max
            return weatherMoEntity
        }
        return nil
    }
    
    private func clearCoreData() {
        do {
            
            let context = CoreDataStack.sharedInstance.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: String(describing: WeatherMO.self))
            do {
                let objects  = try context.fetch(fetchRequest) as? [NSManagedObject]
                _ = objects.map{$0.map{context.delete($0)}}
                CoreDataStack.sharedInstance.saveContext()
            } catch let error {
                print("ERROR DELETING : \(error)")
            }
        }
    }
    

}

extension ViewController : NSFetchedResultsControllerDelegate {
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        
        switch type {
        case .insert:
            self.weatherTableView.insertRows(at: [newIndexPath!], with: .automatic)
        case .delete:
            self.weatherTableView.deleteRows(at: [indexPath!], with: .automatic)
        default:
            break
        }
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.weatherTableView.endUpdates()
    }
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.weatherTableView.beginUpdates()
    }
}


class CustomNavigationController: UINavigationController {
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
}

extension UIColor {
    static let mainTextBlue = UIColor.rgb(r: 7, g: 71, b: 89)
    static let highlightColor = UIColor.rgb(r: 50, g: 199, b: 242)
    
    static func rgb(r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r/255, green: g/255, blue: b/255, alpha: 1)
    }
}
