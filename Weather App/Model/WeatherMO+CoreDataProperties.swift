//
//  WeatherMO+CoreDataProperties.swift
//  
//
//  Created by Tushar Ramoliya on 14/05/19.
//
//

import Foundation
import CoreData


extension WeatherMO {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<WeatherMO> {
        return NSFetchRequest<WeatherMO>(entityName: "WeatherMO")
    }

    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var icon: String?
    @NSManaged public var main: String?
    @NSManaged public var temp: Double
    @NSManaged public var pressure: Int64
    @NSManaged public var humidity: Int64
    @NSManaged public var temp_min: Double
    @NSManaged public var temp_max: Double

}
